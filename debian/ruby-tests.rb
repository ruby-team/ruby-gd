require 'GD'
require 'test/unit'

class GDTest < Test::Unit::TestCase

  attr_reader :p

  def setup
    @p = GD::Polygon.new
    p.addPt(1,1)
    p.addPt(1,2)
    p.addPt(2,1)
    p.addPt(2,2)
  end

  def test_polygon_length
    assert_equal 4, p.length
  end

  def test_polygon_bounds
    assert_equal [1,1,2,2], p.bounds
  end

  def test_polygon_vertices
    assert_equal [[1,1],[1,2],[2,1], [2,2]], p.vertices
  end

end

